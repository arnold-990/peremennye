﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task5 : MonoBehaviour
{
    public int value1;
    public float value2;
    void Start()
    {
        value1 = 3;
        value2 = Mathf.Pow(value1, value1);
        Debug.Log(value2);
    }
}